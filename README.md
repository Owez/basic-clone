# basic-clone

## About

`basic-clone` is a very minimal implamentation of the [BASIC](https://en.wikipedia.org/wiki/BASIC) programming language in Python to practise creating programming languages. The reference tutorial I used can be found [here](https://www.youtube.com/watch?v=Eythq9848Fg).

## Tech

- Python
  - SLY for lexer
- Interpreted
