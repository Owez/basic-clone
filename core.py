"""
Core of basic-clone
"""


class Config:
    """
    Config class, contains constants and other
    """

    class LexerTypes:
        """
        Contains types for the Token class
        """

        INT = "INT"
        FLOAT = "FLOAT"
        PLUS = "PLUS"
        MINUS = "MINUS"
        MUL = "MUL"
        DIV = "DIV"
        LPAREN = "LPAREN"
        RPAREN = "RPAREN"

    class LexerChars:
        """
        Chars to check for the make_tokens method
        """

        WHITESPACE = [" ", "\t"]
        PLUS = ["+"]
        MINUS = ["-"]
        DIV = ["/"]
        MUL = ["*"]
        LPAREN = ["("]
        RPAREN = [")"]

    class LexerChecks:
        """
        All checks for things like DIGITs
        """

        DIGIT = str([0, 1, 2, 3, 4, 5, 6, 7, 8, 9])


config = Config()


class Token:
    """
    Token representation for the Lexer class
    """

    def __init__(self, type_, value):
        self.type = type_
        self.value = value

    def __repr__(self):
        if self.value:
            return f"Token({self.type}, {self.value})"
        else:
            return f"Token({self.type})"


class Lexer:
    """
    A barebones lexer class
    """

    def __init__(self, text):
        self.text = text
        self.pos = -1  # by default, it always increments atleast once
        self.cur_char = None  # by default

    def advance(self):
        """
        Advances to next character in text
        """

        self.pos += 1

        if self.pos < len(self.text):
            self.cur_char = self.text[pos]
        else:
            self.cur_char = None

    def make_tokens(self):
        """
        Iterate through all characters and find what is what
        """

        tokens_out = []
        lexer_chars = self.get_lexerchars_config()
        lexer_types = self.get_lexertypes_config()
        lexer_checks = self.get_lexerchecks_config()

        while self.cur_char:
            if self.cur_char in lexer_chars.WHITESPACE:
                """
                If the cur char is whitespace, advance to next char
                """

                self.advance()
            elif self.cur_char in lexer_checks.DIGIT:
                """
                If the cur char is a digit
                """

                tokens_out.append(self.check_num(cur_char), self.cur_char)
            elif self.cur_char in lexer_chars.PLUS:
                """
                If the cur char is a plus symbol
                """

                tokens_out.append(Token(lexer_types.PLUS, self.cur_char))
                self.advance()
            elif self.cur_char in lexer_chars.MINUS:
                """
                If the cur char is a minus symbol
                """

                tokens_out.append(Token(lexer_types.MINUS, self.cur_char))
                self.advance()
            elif self.cur_char in lexer_chars.MUL:
                """
                If the cur char is a multiply symbol
                """

                tokens_out.append(Token(lexer_types.MUL, self.cur_char))
                self.advance()
            elif self.cur_char in lexer_chars.DIV:
                """
                If the cur char is a divide symbol
                """

                tokens_out.append(Token(lexer_types.DIV, self.cur_char))
                self.advance()
            elif self.cur_char in lexer_chars.LPAREN:
                """
                If the cur char is a `(` symbol
                """

                tokens_out.append(Token(lexer_types.LPAREN, self.cur_char))
                self.advance()
            elif self.cur_char in lexer_chars.RPAREN:
                """
                If the cur char is a `)` symbol
                """

                tokens_out.append(Token(lexer_types.RPAREN, self.cur_char))
                self.advance()

    def check_num(self, cur_char):
        """
        Checks the type of the current digit
        """

        num_str = ""
        dot_count = 0

        pass # @ 6.22 in video

    def get_lexerchars_config(self):
        return config.LexerChars()

    def get_lexertypes_config(self):
        return config.LexerTypes()

    def get_lexerchecks_config(self):
        return config.LexerChecks()
